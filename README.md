# QA-Experta

Evaluación Técnica Experta Seguros
---------------------------------

dependencias:
---------------------------------

python : https://www.python.org/
selenium webdriver : https://www.selenium.dev/downloads/
Chrome

Detalle de los casos:
---------------------------------

TestCase_01:
- Seleccionar expera online.
- Darle a la opcion personas.
- Ingresar un usuario y contraseña inexistentes.
- Luego hacer click en iniciar sesion.
- Verificar el mensaje de error "La contraseña ingresada es incorrecta.".

TestCase_02:
- Seleccionar expera online.
- Seleccionar olvide mi contraseña.
- Ingresar un usuario inexistente.
- Luego hacer click en recuperar.
- Verificar el mensaje de error "El usuario es incorrecto.".

Funcionalidades de este tipo sirven para verificar el correcto funcionamiento del login.

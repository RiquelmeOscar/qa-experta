#!/urs/bin/python
#-*- coding; utf-8 -*-
import os, sys
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

class TestCase(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('chromedriver.exe')
        self.driver.maximize_window()
        self.driver.get('https://www.experta.com.ar/')
    
    def testCase01(self): 
        try:
            # traemos los datos
            self.getData()

            # abrimos y seteamos el navegador al maximo
            browser = self.driver

            # esperamos a que cargue el login
            element = WebDriverWait(browser, 10).until(
                expected_conditions.presence_of_element_located((By.ID, "btn-experta-online"))
            )
            time.sleep(5)

            # Seleccionar Experta Online
            browser.find_element(By.XPATH, "//button[contains(@id,'btn-experta-online')]").click()
            time.sleep(5)

            # Seleccionar olvide mi contraseña
            btn_login = browser.find_element(By.XPATH, "//a[contains(@id,'recover-user')]").click()
            time.sleep(5)

            # Ingresar DNI
            browser.find_element(By.XPATH, "//form[contains(@id,'formResetPassword')]//input[contains(@name,'value(USUARIO)')]").send_keys(self.userDni)
            time.sleep(5)
            
            # seleccionar recuperar
            browser.find_element(By.XPATH, "//button[contains(@id,'resetPassword')]").click()
            time.sleep(5)

            # Cambiamos de ventana
            browser.switch_to.window(browser.window_handles[1])
            time.sleep(5)

            # Verificamos el error
            error_txt = browser.find_element(By.XPATH, "//*[contains(@class,'errorTable')]//li[contains(.,'El usuario es incorrecto')]")
            parent_error = error_txt._parent
            parent_error.execute_script("arguments[0].setAttribute('style', arguments[1])", error_txt, "border: 4px solid red")
            time.sleep(5)

        finally:
            browser.quit()

    def tearDown(self):
        self.driver.quit()

    def getData(self):
        # data
        self.userDni = "45216389"
        
if __name__ == '__main__':
    unittest.main(verbosity=2)